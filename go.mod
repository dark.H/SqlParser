module gitee.com/dark.H/SqlParser

go 1.14

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/c-bata/go-prompt v0.2.3
	github.com/fatih/color v1.9.0
	github.com/go-openapi/strfmt v0.19.5 // indirect
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942 // indirect
	github.com/tealeg/xlsx v1.0.5
)
