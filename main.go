package main

import (
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	"gitee.com/dark.H/SqlParser/sql"
)

var (
	FilePath string
	Tp       string
)

func main() {
	flag.StringVar(&FilePath, "f", "", "file path ")
	flag.StringVar(&Tp, "t", "mssql", "sql file tp ")
	flag.Parse()
	st := time.Now()
	hand, err := sql.LoadSql(FilePath)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Used time:", time.Now().Sub(st))
	log.Println("Create Lines:", len(hand.CreateLines))
	log.Println("INsert Lines:", len(hand.InsertLines))
	log.Println("Other Lines:", len(hand.OtherLines))
	log.Println("")
	if tbKey := sql.CompletedInput(hand.GetTables()...); tbKey != "" {
		tbs := strings.Fields(tbKey)
		hand.SetFilterTable(tbs...)
	}

	hand.ParserLeft()
	// runtime.GC()
	for name, v := range hand.Datas {
		fmt.Println(name, ":", len(v))
	}
	sql.CliCmd(hand)
	// log.Println("Create Lines:", sql.CreateLines)

}
