package sql

import (
	"log"
	"strconv"
	"strings"
)

func (sql *SqlOper) MySqlValue(onedict Dict, tp, k, v string) (err error) {
	if strings.HasPrefix(tp, "float") {
		if v == "NULL" {
			onedict[k] = 0.0
		} else {
			onedict[k], err = strconv.ParseFloat(v, 32)
			if err != nil {
				log.Println("Parse int error:", k, tp, v, "|", err)
			}
		}

	} else if strings.HasPrefix(tp, "double") {
		if v == "NULL" {
			onedict[k] = 0.0
		} else {
			onedict[k], err = strconv.ParseFloat(v, 32)
			if err != nil {
				log.Println("Parse int error:", k, tp, v, "|", err)
			}
		}

	} else if strings.HasPrefix(tp, "int") {
		if v == "NULL" {
			onedict[k] = 0
		} else {
			v = strings.Replace(v, "\n", "", -1)
			onedict[k], err = strconv.Atoi(v)
			if err != nil {
				return
			}
		}
	} else if strings.HasPrefix(tp, "bit") {
		if v == "0" {
			onedict[k] = false
		} else {
			onedict[k] = true
		}
	} else {
		if v[0] == '\'' {
			v = v[1 : len(v)-1]
		}
		if v == "NULL" {
			onedict[k] = ""
		} else {
			onedict[k] = v
		}
	}
	return
}
