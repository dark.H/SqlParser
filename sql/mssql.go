package sql

import (
	"log"
	"strconv"
	"strings"
)

func (sqloper *SqlOper) MsSqlValue(onedict Dict, tp, k, v string) (err error) {

	if strings.HasPrefix(tp, "decimal") {
		if v == "NULL" {
			onedict[k] = 0.0
		} else if v == "" {
			onedict[k] = 0.0
		} else {
			onedict[k], err = strconv.ParseFloat(v[2:len(v)-1], 32)
			if err != nil {
				log.Println("Parse int error:", k, tp, v, "|", err)
			}
		}

	} else if strings.HasPrefix(tp, "int") {
		if v == "NULL" {
			onedict[k] = 0
		} else if v == "" {
			onedict[k] = 0
		} else {
			onedict[k], err = strconv.Atoi(v[2 : len(v)-1])
			if err != nil {
				log.Println("Parse int error:", k, tp, v[2:len(v)-1], "|", err)
			}
		}
	} else if strings.HasPrefix(tp, "bit") {
		if v == "N'0'" {
			onedict[k] = true
		} else {
			onedict[k] = false
		}
	} else {
		if v == "NULL" {
			onedict[k] = ""
		} else if v == "N''" {
			onedict[k] = ""
		} else {
			if len(v)-1 < 2 {
				log.Println("Error Range in :", v)
			}
			onedict[k] = v[2 : len(v)-1]
		}
	}
	// log.Fatal(onedict)
	return
}
