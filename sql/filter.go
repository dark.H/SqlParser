package sql

import (
	"fmt"
)

func (table Table) Query(tableName string, g G, page int, pageLen int) (O []Dict) {
	cursor := 0
	start := page * pageLen
	end := start + pageLen
	for _, t := range table[tableName] {
		found := true
		for k, v := range g {
			if fmt.Sprint(t[k]) != v {
				found = false
				break
			}
		}
		if found {
			if cursor >= end {
				break
			}
			if cursor >= start {
				O = append(O, t)
			}
			cursor++
		}
	}
	return
}
